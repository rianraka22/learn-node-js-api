function responCheck(data, res) {
    data.then((reqData) => {
        if (reqData.length == 0) {
            code(404, reqData, res)
        } else {
            code(200, reqData, res)
        }
    })
}

const code = (responCode, data, res) => {
    switch (responCode) {
        case 200:
            return res.status(200).json({
                'responCode': 200,
                'reqData': data
            })
            break;

        case 404:
            return res.status(404).json({
                'responCode': 404,
                'Msg': "Data Not Found"
            })
            break;
    }
}

module.exports = { responCheck }