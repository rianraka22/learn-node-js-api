const express = require('express')
const app = express()
const port = 3000

const Cuser = require('../controllers/C.user')

app.get('/', (req, res) => {
    try {
        Cuser.Readuser(res)
    } catch (error) {
        console.log(error);
    }
})

app.get('/getId/:id', (req, res) => {
    try {
        Cuser.Getuser(req.params.id, res)
    } catch (error) {
        console.log(error);
    }
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})