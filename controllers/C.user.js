const user = require('../models/M.user')
const respon = require('../utils/respon.code')

function Readuser(res) {
    let data = user.getTes()
    respon.responCheck(data, res)
}

function Getuser(id, res) {
    let data = user.getTesNama(id)
    respon.responCheck(data, res)
}

module.exports = { Readuser, Getuser }