const { knex } = require('./db')

const getTes = async() => {
    return await knex('tb_dummy')
}

//RETURN harus 
const getTesNama = async(id) => (
    await knex('tb_node').where('id', id)
)

const insertTes = async(nama, hobi, tgl_lahir) => {
    await knex('tb_node').insert({
        "nama": nama,
        "hobi": hobi,
        "tgl_lahir": tgl_lahir
    })
}

const deleteTes = async(id) => {
    await knex('tb_node').where('id', id).del()
}

const updateTes = async(id, nama, hobi, tgl_lahir) => {
    await knex('tb_node').where('id', id).update({
        "nama": nama,
        "hobi": hobi,
        "tgl_lahir": tgl_lahir
    })
}

module.exports = { insertTes, deleteTes, updateTes, getTes, getTesNama }